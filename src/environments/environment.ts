// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyARs_zLjgkDk2wcSht6SYkywh9GBJP1_5c',
    authDomain: 'travel-doodle.firebaseapp.com',
    databaseURL: 'https://travel-doodle.firebaseio.com',
    projectId: 'travel-doodle',
    storageBucket: 'travel-doodle.appspot.com',
    messagingSenderId: '544076173964'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
